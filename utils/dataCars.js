const fs = require("fs");

const loadData = () => {
  const fileBuffer = fs.readFileSync("./data/data.json", "utf-8");
  const dataCars = JSON.parse(fileBuffer);
  return dataCars;
};

module.exports = { loadData };

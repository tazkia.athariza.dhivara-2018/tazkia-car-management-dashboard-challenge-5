# Tazkia - Car Management Dashboard - Challenge 5

# Car Management Dashboard
"Binar Car Rental" was created for academic purpose, namely Challenge Chapter 5 in Binar Academy - Fullstack Web Course. The aims of this challenge are to implement database, restAPI, ORM, etc.

## About
This website contains three major pages and built by html (written in ejs format), CSS, Bootstrap v5.0.2 (online ver), and Javascript. For database, this website is using postgreSQL. Server was created by express and ejs as view engine. 

## Getting Started
You would need to install several third party modules (express, ejs, ejs-layout, sequelize, etc) before running the program. After that, you can run the program from your terminal by using this command : 'node index.js'. 

## Version History
Commit changes can be checked on https://gitlab.com/tazkia.athariza.dhivara-2018/tazkia-car-management-dashboard-challenge-5/-/commits/main

## Author
Tazkia Athariza Dhivara

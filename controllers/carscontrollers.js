/* const { carsdata } = require('../models);

 carsdata.create({
    carType: 'updated',
    carPrice: 270996,
    carYear: 1996,
    carImage: '../image/car26.png',
    carSize: 'Medium'
})
.then(carsdata => {
    console.log(carsdata)
}); */

const carsservice = require("../services/carsservice");

const getCars = async (req, res) => {
  const getCars = await carsservice.getCars();
  return getCars;
};

const createCars = async (req, res) => {
  const { carType, carPrice, carImage, carSize } = req.body;
  const createCars = await carsservice.createCars({
    carType,
    carPrice,
    carImage,
    carSize,
  });
  res.redirect("/");
};

const editCarsById = async (req, res) => {
  const { carType, carPrice, carImage, carSize } = req.body;
  const { id } = req.params;
  const editCarsById = await carsservice.editCarsById({
    id,
    carType,
    carPrice,
    carImage,
    carSize,
  });
  res.redirect("/");
};

const renderCarById = async (req, res) => {
  const { id } = req.params;
  const car = await carsservice.getById({ id });
  res.render("pages/editCars", {
    layout: "layouts/main-layout",
    title: "Update Car Information",
    car: car,
  });
};

const getCarsAll = async (req, res) => {
  const getCars = await carsservice.getCars();
  res.render("pages/carList", {
    layout: "layouts/main-layout",
    title: "Car List",
  });
};

const deleteCarsById = async (req, res) =>{
  const { id } = req.params;
  const deleteCarsById = await carsservice.deleteCarsById({ id });
  res.redirect("/")
}

module.exports = { getCars, createCars, editCarsById, renderCarById, getCarsAll, deleteCarsById };

/* module.exports = {
    create: async (req, res) => {
        try {
            const body = req.body;
            const data = await carsdata.create(body);
            return res.status(200).json({
                success: true,
                error: false,
                data: data,
                message: "Data was successfully created!",
            });
            } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                data: null,
                message: error,
            })
        }
    },

    list: async (req, res) => {
        try {
            const data = await carsdata.findAll();
            return res.status(200).json({
                success: true,
                error: false,
                data: data,
                message: "Data was successfully populated!",
            });
            } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                data: null,
                message: error,
            })
        }
    },

    destroy: async (req, res) => {
        try {
            const data = await carsdata.destroy({where:{id:req.params.id}});
            return res.status(200).json({
                success: true,
                error: false,
                data: data,
                message: "Data deleted!",
            });
            } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                data: null,
                message: error,
            })
        }
    },

    update: async (req, res) => {
        try {
            const data = await carsdata.update(body, {where:{id:req.params.id}});
            return res.status(200).json({
                success: true,
                error: false,
                data: data,
                message: "Data was successfully updated!",
            });
            } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                data: null,
                message: error,
            })
        }
    },
}; */
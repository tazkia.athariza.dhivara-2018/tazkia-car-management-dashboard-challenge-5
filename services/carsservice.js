const carsrepository = require("../repositories/carsrepository");

class carsservice {
  static async getCars() {
    const getCars = await carsrepository.getCars();
    return getCars;
  }

  static async createCars({ carType, carPrice, carImage, carSize }) {
    const createCars = await carsrepository.createCars({
        carType,
        carPrice,
        carImage,
        carSize,
    });
    return createCars;
  }

  static async getById({ id }) {
    const getByCarsId = await carsrepository.getById({ id });
    return getByCarsId;
  }

  static async editCarsById({ id, carType, carPrice, carImage, carSize }) {
    const editCarsById = await carsrepository.editCarsById({
      id,
      carType,
      carPrice,
      carImage,
      carSize,
    });
    return editCarsById;
  }

  static async deleteCarsById({ id }) {
    const deleteCarsById = await carsrepository.deleteCarsById({
      id,
    });
    return deleteCarsById;
  }

}

module.exports = carsservice;

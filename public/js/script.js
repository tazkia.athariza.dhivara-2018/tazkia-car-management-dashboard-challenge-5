// for displaying or closing the sidebar on pages
let menu_btn = document.querySelector("#menu-btn");
let sidebar = document.querySelector("#sidebar");
let content = document.querySelector(".my-container");

menu_btn.addEventListener("click", () => {
    sidebar.classList.toggle("active-nav");
    content.classList.toggle("active-cont");
});

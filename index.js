const express = require("express");
const expressLayouts = require("express-ejs-layouts");
const bodyParser = require("body-parser");
const path = require("path");
const carscontrollers = require("./controllers/carscontrollers");
const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded());
app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static(__dirname + "/public"));

app.post("/cars", carscontrollers.createCars);
app.post("/cars/:id", carscontrollers.editCarsById);
app.post("/deleteCars/:id", carscontrollers.deleteCarsById);

app.get("/", async (req, res) => {
  const getCars = await carscontrollers.getCars();
  res.render("pages/carList", {
    layout: "layouts/main-layout",
    title: "Car List",
    cars: getCars,
  });
});

app.get("/cars", (req, res) => {
  res.render("pages/addCars", {
    layout: "layouts/main-layout",
    title: "Add New Car",
  });
});

app.get("/update/:id", carscontrollers.renderCarById);

app.listen(PORT, () => {
  console.log(`Server is now running on http://localhost:${PORT}`);
});
